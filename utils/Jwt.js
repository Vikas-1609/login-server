const jwt = require('jsonwebtoken');
const { getEnvironmentVariables } = require('../environments/environment');

class Jwt {
  static jwtSign(payload, expires_in = '180d') {
    return jwt.sign(
      payload,
      getEnvironmentVariables().jwt_secret_key,
      { expiresIn: expires_in, issuer: 'technyks.com' }
    );
  }

  static async jwtVerify(token) {
    try {
      const decoded = await new Promise((resolve, reject) => {
        jwt.verify(token, getEnvironmentVariables().jwt_secret_key, (err, decoded) => {
          if (err) reject(err);
          else if (!decoded) reject(new Error('Token verification failed'));
          else resolve(decoded);
        });
      });
      return decoded;
    } catch (error) {
      throw error;
    }
  }
}

module.exports = Jwt;
