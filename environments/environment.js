const DevEnvironment = require('./environment.dev');
const ProdEnvironment = require('./environment.prod');

function getEnvironmentVariables() {
    if (process.env.NODE_ENV === 'production') {
        return ProdEnvironment;
    }
    return DevEnvironment;
}

module.exports = { getEnvironmentVariables };
