const { User } = require("../Models/User");
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
    const login = async(req, res) => {
      console.log(req)
      try {
        const { email, password } = req.body
        if (email && password) {
            const user = await User.findOne({ email: email })
            if (user != null) {
                const isMatch = await bcrypt.compare(password, user.password)
                if ((user.email === email) && isMatch) {
                    console.log("Login Successful", req.body)
                    res.send({ "status": "success", "message": "Login Success"})
                } else {
                    res.send({ "status": "failed", "message": "Username or Password is not Valid" })
                }
            } else {
                res.send({ "status": "failed", "message": "You are not a Registered User" })
            }
        } else {
            res.send({ "status": "failed", "message": "All Fields are Required" })
        }
    } catch (error) {
        console.log(error)
        res.send({ "status": "failed", "message": "Unable to Login" })
    }
  }

module.exports = login;