const UserValidators = require('./../validators/UserValidators');
const express = require('express');
const login = require('./../validators/UserValidators');

class UserRouter {
  constructor() {
    this.router = express.Router();
    this.setupRoutes();
  }

  setupRoutes() {
    // POST /user/login
    this.router.post('/login',login);
  }
}

module.exports = new UserRouter().router;
