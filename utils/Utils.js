const Bcrypt = require('bcrypt');
const Multer = require('multer');
class Utils {
  constructor() {
    this.multer = Multer({ storage: storageOptions, fileFilter: fileFilter });
  }

  static async encryptPassword(password) {
    try {
      const hash = await Bcrypt.hash(password, 10);
      return hash;
    } catch (error) {
      throw error;
    }
  }

  static async comparePassword(data) {
    try {
      const same = await Bcrypt.compare(data.password, data.encrypt_password);
      return same;
    } catch (error) {
      throw error;
    }
  }
}

module.exports = Utils;
