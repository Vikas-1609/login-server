const express = require('express');
const Server = require('./server');
const server = new Server().app;
const port = 4000;

server.listen(port, () => {
    console.log(`Server is running at port ${port}`);
}).on('error', error => {
    console.error('Server startup error:', error);
});
