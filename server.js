const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const { getEnvironmentVariables } = require('./environments/environment');
const UserRouter = require('./Routes/Route');
const express = require('express');

class Server {
  constructor() {
    this.app = express();
    this.setConfigs();
    this.setRoutes();
    this.error404Handler();
    this.handleErrors();
  }

  setConfigs() {
    this.app.use(cors({
        origin: 'http://localhost:3000' // Replace with your frontend's URL
      }));
  }
  setConfigs() {
    this.connectMongoDB();
    this.allowCors();
    this.configureBodyParser();
  }

  connectMongoDB() {
    mongoose.connect(getEnvironmentVariables().db_uri)
      .then(() => {
        console.log('Connected to mongodb.');
      })
      .catch(error => {
        console.error('MongoDB connection error:', error);
      });
  }

  configureBodyParser() {
    this.app.use(bodyParser.urlencoded({ extended: true }));
    this.app.use(bodyParser.json()); // Uncomment for handling JSON data
  }

  allowCors() {
    this.app.use(cors());
  }

  setRoutes() {
    this.app.use('/api/user', UserRouter);
  }

  error404Handler() {
    this.app.use((req, res) => {
      res.status(404).json({
        message: 'Not found',
        status_code: 404
      });
    });
  }

  handleErrors() {
    this.app.use((error, req, res, next) => {
      const errorStatus = req.errorStatus || 500;
      res.status(errorStatus).json({
        message: process.env.NODE_ENV === 'development' ? error.message : 'Something went wrong. Please try again!',
        status_code: errorStatus
      });
    });
  }
}

module.exports = Server;
